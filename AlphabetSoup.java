import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class AlphabetSoup {

  private static int xDim, yDim;

  public boolean getFileSearch(String filePath) {
    boolean retStatus = false;
    Path path = Paths.get(filePath);

    try {
      List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
      retStatus = true;
      
      String[] arr = lines.toArray(new String[0]);

      processDims(arr[0]);
      String[][] grid = new String[xDim][yDim];
      grid = processGrid(arr, xDim, yDim);
      
      String[] searchedWords = new String[arr.length-yDim-1]; 
      for(int i=0; i<searchedWords.length; i++) {
        searchedWords[i] = arr[i+yDim+1];
      }

      int happens = searchHorizon(grid, searchedWords);    
      happens += searchVertical(grid, searchedWords);
      happens += searchDiagonal(grid, searchedWords);
      
      return retStatus;
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return retStatus;
  } 
  
  private int searchDiagonal(String[][] grid, String[] patterns) {
	  
    int happens= 0;
	String[] str = new String[xDim];
	StringBuilder sb = new StringBuilder();
		
	for (int j=0; j<yDim; j++) sb.append(grid[j][j].toUpperCase());
		  
	for (int k=0; k<patterns.length; k++) {
	  Pattern p = Pattern.compile(patterns[k]);
	  Matcher m = p.matcher(sb.toString());
	  while(m.find()){
		System.out.println(patterns[k] + " " + m.start() + ":" + m.start() + " " + (m.end()-1) + ":" + (m.end()-1));
		System.out.println(" ");
		happens++;
	  }
		     
	  Matcher reverseM = p.matcher(sb.reverse().toString());
	  while(reverseM.find()){
		System.out.println(patterns[k] + " " + (xDim-reverseM.start()-2) + ":" + (xDim-reverseM.start()-2) + " " + (xDim-reverseM.end()) + ":" + (xDim-reverseM.end()));
	    happens++;
	  }
	}		
	return happens;
  }
  
  private int searchVertical(String[][] grid, String[] patterns) {
	  
		int happens= 0;
		String[] str = new String[yDim];
		StringBuilder sb = new StringBuilder();
		
		for (int i=0; i<xDim; i++) {
		  for (int j=0; j<yDim; j++) {
			sb.append(grid[i][j].toUpperCase());
		  }
		  for (int k=0; k<patterns.length; k++) {
		    Pattern p = Pattern.compile(patterns[k]);
		    Matcher m = p.matcher(sb.toString());
		    while(m.find()){
		      System.out.println(patterns[k] + " " + i +":" + m.start()  + " " + i + ":" + (m.end()-1));
		      happens++;
		    }
		     
		    Matcher reverseM = p.matcher(sb.reverse().toString());
		    while(reverseM.find()){
			  System.out.println(patterns[k] + " " + i + ":" + (xDim-reverseM.start()-1) + " " + i + ":" + (xDim-reverseM.end()));
			  happens++;
		    }
		    sb.reverse();
		 }
		  sb.setLength(0);
	   }

		return happens;
	  }
  
  private int searchHorizon(String[][] grid, String[] patterns) {
	  
	int happens= 0;
	String[] str = new String[xDim];
	StringBuilder sb = new StringBuilder();
	
	for (int j=0; j<yDim; j++) {
	  for (int i=0; i<xDim; i++) {
		sb.append(grid[i][j].toUpperCase());
	  }
	  for (int k=0; k<patterns.length; k++) {
	    Pattern p = Pattern.compile(patterns[k]);
	    Matcher m = p.matcher(sb.toString());
	    while(m.find()){
	      System.out.println(patterns[k] + " " + j + ":" + m.start() + " " + j + ":" + (m.end()-1));
	      happens++;
	    }
	     
	    Matcher reverseM = p.matcher(sb.reverse().toString());
	    while(reverseM.find()){
	      //int xposStart = 
		  System.out.println(patterns[k] + " " + j + ":" + (xDim-reverseM.start()-1) + " " + j + ":" + (xDim-reverseM.end()));
		  happens++;
	    }
	   sb.reverse();
	 }
	  sb.setLength(0);
   }
	
	return happens;
  }

  private String[][] processGrid(String[] arr, int xDim, int yDim){
    String[][] grid = new String[xDim][yDim]; 
    for (int j=0; j<yDim; j++) {
      String[] line = arr[j+1].split("\\s+");
      
      for (int i=0; i<xDim; i++) {
        
        grid[i][j] = line[i];

      }
    }
    return grid;
    
  }

  private void processDims(String line0) {

      String[] dims = line0.split("x|X");
      xDim = Integer.parseInt(dims[0]);
      yDim = Integer.parseInt(dims[1]);
  }

  public static void main(String... args ) {
    
    AlphabetSoup alphabetSoup = new AlphabetSoup();

    //Use absolute path to invoke getFileInfo
    boolean ret0 = alphabetSoup.getFileSearch(args[0]);
      System.out.println("main xDim is "+ xDim + " yDim is "+ yDim);
    System.out.println("Return status is " + ret0);
  }

}
